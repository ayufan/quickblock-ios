//
//  GameSettings.h
//  QuickBlock
//
//  Created by ayufan on 11/4/08.
//  Copyright 2008 BinaryCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameResult : NSObject {
	NSString* name;
	float time;
	unsigned score;
}
@property (copy) NSString* name;
@property (assign) float time;
@property (assign) unsigned score;
@end

@interface GameSettings : NSObject {
	NSString* name;
	NSArray* results;
}
@property (copy) NSString* name;
@property (retain) NSArray* results;

- (void)addResultForName:(NSString*)name Time:(float)time Score:(unsigned)score;
@end

