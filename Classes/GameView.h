//
//  GameView.h
//  QuickBlock
//
//  Created by ayufan on 9/30/08.
//  Copyright 2008 BinaryCore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameView : UIViewController {
	double startTime;
	double lastTime;
	UIImageView* player;
	bool inGame;
	CGPoint draggedPoint;
	BOOL dragged;
	unsigned pointCount;
	UIAlertView* alertView;
	
	IBOutlet UIImageView* pointDigit0;
	IBOutlet UIImageView* pointDigit1;
	IBOutlet UIImageView* pointDigit2;
	
	IBOutlet UIImageView* timeSecondsDigit0;
	IBOutlet UIImageView* timeSecondsDigit1;
	IBOutlet UIImageView* timeMinutesDigit0;
	IBOutlet UIImageView* timeMinutesDigit1;
}

@end
