//
//  QuickBlockAppDelegate.h
//  QuickBlock
//
//  Created by ayufan on 9/30/08.
//  Copyright BinaryCore 2008. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuView;
@class GameView;
@class CreditsView;
@class GameSettings;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	IBOutlet UIWindow *window;
	IBOutlet MenuView *menuView;
	IBOutlet GameView *gameView;
	IBOutlet CreditsView *creditsView;
	GameSettings* settings;
}

@property (readonly) GameSettings* settings;

- (IBAction)play;
- (IBAction)credits;
- (IBAction)menu;

+ (AppDelegate*)sharedInstance;
@end

