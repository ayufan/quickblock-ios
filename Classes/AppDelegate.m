//
//  QuickBlockAppDelegate.m
//  QuickBlock
//
//  Created by ayufan on 9/30/08.
//  Copyright BinaryCore 2008. All rights reserved.
//

#import "AppDelegate.h"
#import "GameSettings.h"
#import "MenuView.h"
#import "GameView.h"
#import "CreditsView.h"
#import "Common.h"

@implementation AppDelegate
@synthesize settings;

- (void)applicationDidFinishLaunching:(UIApplication *)application {
	// Load results
	settings = [[NSKeyedUnarchiver unarchiveObjectWithFile:SETTINGS_FILE] retain];
	if(!settings)
		settings = [[GameSettings alloc] init];
	
	// Configure view to fullscreen
	application.statusBarHidden = YES;
	menuView.view.frame = [UIScreen mainScreen].bounds;
	creditsView.view.frame = [UIScreen mainScreen].bounds;
	gameView.view.frame = [UIScreen mainScreen].bounds;
	
	// Make active
	[window makeKeyAndVisible];
	[window addSubview:menuView.view];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Save settings
	if(settings)
		[NSKeyedArchiver archiveRootObject:settings toFile:SETTINGS_FILE];
}

- (void)dealloc {
	[menuView release];
	[gameView release];
	[creditsView release];
	[window release];
	[settings release];
	[super dealloc];
}

- (void)switchToView:(UIViewController*)toView fromView:(UIViewController*)fromView {
	// Begin animations
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0];
	[UIView setAnimationTransition:(fromView == menuView ? UIViewAnimationTransitionFlipFromLeft : UIViewAnimationTransitionFlipFromRight) forView:window cache:YES];
	//[fromView viewWillDisappear:YES];
	//[toView viewWillAppear:YES];
	
	// Switch to login view
	[fromView.view removeFromSuperview];
	[window addSubview:toView.view];
	
	// Finish animations
	//[fromView viewDidDisappear:YES];
	//[toView viewDidAppear:YES];
	[UIView commitAnimations];	
}

- (IBAction)play {
	[self switchToView:gameView fromView:menuView];
}

- (IBAction)credits {
	[self switchToView:creditsView fromView:menuView];
}
	
- (IBAction)menu {
	if(gameView.view.superview == window)
		[self switchToView:menuView fromView:gameView];
	else if(creditsView.view.superview == window)
		[self switchToView:menuView fromView:creditsView];
	else
		[self switchToView:menuView fromView:nil];
}

+ (AppDelegate*)sharedInstance {
	return (AppDelegate*)[UIApplication sharedApplication].delegate;
}
@end
