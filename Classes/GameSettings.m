//
//  GameSettings.m
//  QuickBlock
//
//  Created by ayufan on 11/4/08.
//  Copyright 2008 BinaryCore. All rights reserved.
//

#import "Common.h"
#import "GameSettings.h"

@implementation GameResult
@synthesize name, score, time;

- (id)init {
	loge();
	if(self = [super init]) {
	}
	return self;
}

- (void)dealloc {
	loge();
	[name release];
	[super dealloc];
}

- (id)initWithCoder:(NSCoder*)coder {
	loge();
	if(self = [super init]) {
		self.name = [coder decodeObjectForKey:@"Name"];
		self.time = [coder decodeFloatForKey:@"Time"];
		self.score = [coder decodeIntForKey:@"Score"];
	}
	return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
	[coder encodeObject:name forKey:@"Name"];
	[coder encodeFloat:time forKey:@"Time"];
	[coder encodeInt:score forKey:@"Score"];
}

- (int)resultComparer:(GameResult*)other {
	if(score > other.score)
		return -1;
	if(score < other.score)
		return 1;
	if(time > other.time)
		return -1;
	if(time < other.time)
		return 1;
	return 0;
}
@end

@implementation GameSettings
@synthesize name, results;

- (id)init {
	loge();
	if(self = [super init]) {
		results = [[NSArray array] retain];
	}
	return self;
}

- (void)dealloc {
	loge();
	[name release];
	[results release];
	[super dealloc];
}

- (id)initWithCoder:(NSCoder*)coder {
	loge();
	if(self = [super init]) {
		self.name = [coder decodeObjectForKey:@"Name"];
		self.results = [coder decodeObjectForKey:@"Results"];
	}
	return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
	[coder encodeObject:name forKey:@"Name"];
	[coder encodeObject:results forKey:@"Results"];
}

- (void)addResultForName:(NSString*)playerName Time:(float)time Score:(unsigned)score {
	// Create new result
	GameResult* result = [[[GameResult alloc] init] autorelease];
	result.name = playerName;
	result.time = time;
	result.score = score;
	
	// Create array of results and sort it's content
	NSMutableArray* newResults = [NSMutableArray arrayWithArray:results];
	[newResults addObject:result];
	[newResults sortUsingSelector:@selector(resultComparer:)];
	while(MAX_RESULTS < [newResults count])
		[newResults removeLastObject];
	
	// Save new results
	[results release];
	results = [newResults retain];
}
@end
