//
//  GameView.m
//  QuickBlock
//
//  Created by ayufan on 9/30/08.
//  Copyright 2008 BinaryCore. All rights reserved.
//

#import "GameView.h"
#import "Common.h"
#import "AppDelegate.h"
#import "GameSettings.h"
#include <sys/time.h>

static double getTime() {
	struct timeval tv;
	struct timezone tz;
	if(gettimeofday(&tv, &tz))
		return -1;
	return tv.tv_sec + tv.tv_usec / 1000000.0;
}

static float randRange(float x, float width) {
	return x + ((float)(rand() % RAND_MAX) / RAND_MAX) * width;
}

static float randRangeAlign(float x, float width, float align) {
	float r = randRange(x, width);
	return x + floor((r - x) / align + 0.5f) * align;
}

static CGRect ClipRectToArea(CGRect frame, CGRect area) {
	if(frame.origin.x < area.origin.x)
		frame.origin.x = area.origin.x;
	if(frame.origin.y < area.origin.y)
		frame.origin.y = area.origin.y;
	if(frame.origin.x + frame.size.width > area.origin.x + area.size.width)
		frame.origin.x = area.origin.x + area.size.width - frame.size.width;
	if(frame.origin.y + frame.size.height > area.origin.y + area.size.height)
		frame.origin.y = area.origin.y + area.size.height - frame.size.height;
	return frame;
}

@interface GameOpponent : UIImageView {
	float velX, velY;
}
@property (assign) float velX, velY;
@property (readonly) float posX, posY;
@end

@implementation GameOpponent
@synthesize velX, velY;
@synthesize posX, posY;

- (float)posX {
	return self.frame.origin.x + OP_SIZE / 2;
}

- (float)posY {
	return self.frame.origin.y + OP_SIZE / 2;
}

- (void)setPosX:(float)x Y:(float)y {
	self.frame = ClipRectToArea(CGRectMake(x - OP_SIZE / 2, y - OP_SIZE / 2, OP_SIZE, OP_SIZE), GameArea);
}

+ (GameOpponent*)createAtX:(float)x Y:(float)y {
	GameOpponent* op = [[[GameOpponent alloc] initWithImage:[UIImage imageNamed:@"brickOrange.png"]] autorelease];
	[op setPosX:x Y:y];
	return op;
}
@end

@interface GamePoint : UIImageView {
	float pointTime;
}
@property (readonly) float posX, posY;
@property (assign) float pointTime;
@end

@implementation GamePoint
@synthesize posX, posY, pointTime;

- (float)posX {
	return self.frame.origin.x + POINT_SIZE / 2;
}

- (float)posY {
	return self.frame.origin.y + POINT_SIZE / 2;
}

- (void)setPosX:(float)x Y:(float)y {
	self.frame = ClipRectToArea(CGRectMake(x - POINT_SIZE / 2, y - POINT_SIZE / 2, POINT_SIZE, POINT_SIZE), GameArea);
}

+ (GamePoint*)createAtX:(float)x Y:(float)y {
	GamePoint* pt = [[[GamePoint alloc] initWithImage:[UIImage imageNamed:@"brickBlue.png"]] autorelease];
	[pt setPosX:x Y:y];
	return pt;
}
@end


@implementation GameView
- (void)removeAllOpponents {
	for(GameOpponent* op in self.view.subviews) {
		if([op isKindOfClass:[GameOpponent class]])
			[op removeFromSuperview];
	}
}

- (unsigned)opponents {
	unsigned count = 0;
	for(GameOpponent* op in self.view.subviews) {
		if([op isKindOfClass:[GameOpponent class]])
			++count;
	}
	return count;
}

- (void)removeAllPoints {
	for(GamePoint* pt in self.view.subviews) {
		if([pt isKindOfClass:[GamePoint class]])
			[pt removeFromSuperview];
	}
}

- (unsigned)points {
	unsigned count = 0;
	for(GamePoint* pt in self.view.subviews) {
		if([pt isKindOfClass:[GamePoint class]])
			++count;
	}
	return count;
}

- (void)updatePointCount {
	pointDigit0.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (pointCount / 1) % 10]]; 
	pointDigit1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (pointCount / 10) % 10]]; 
	pointDigit2.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (pointCount / 100) % 10]]; 
}

- (void)updateTimer {
	int currentTime = getTime() - startTime;
	timeSecondsDigit0.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (currentTime / 1) % 10]]; 
	timeSecondsDigit1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (currentTime / 10) % 10]]; 
	timeMinutesDigit0.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (currentTime / 60) % 10]]; 
	timeMinutesDigit1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.png", (currentTime / 600) % 10]]; 
}


- (void)addNewPoint {
	GamePoint* pt = [GamePoint createAtX:randRangeAlign(PointArea.origin.x + POINT_SIZE/2, PointArea.size.width - (POINT_SIZE-1) * 2, POINT_SIZE-1) Y:randRangeAlign(PointArea.origin.y + POINT_SIZE/2, PointArea.size.height - (POINT_SIZE-1) * 2, POINT_SIZE-1)];
	pt.alpha = 0.0f;
	[self.view addSubview:pt];
	
	// Move opponents to front
	for(UIView* iter in self.view.subviews) {
		if([iter isKindOfClass:[GameOpponent class]])
			[self.view bringSubviewToFront:iter];
	}
	
	// Move player to front
	[self.view bringSubviewToFront:player];
}

- (void)addNewOpponent {
	// Find last added opponent
	GameOpponent* op = nil;
	if([self opponents]) {
		for(GameOpponent* iter in self.view.subviews) {
			if([op isKindOfClass:[GameOpponent class]])
				op = iter;
		}
	}
	
	if(op) {
		// Create at last opponent position
		op = [GameOpponent createAtX:op.posX Y:op.posY];
	}
	else {
		// Randomize opponent position
		//do {
		//	op = [GameOpponent createAtX:randRangeAlign(PointArea.origin.x + POINT_SIZE-1, PointArea.size.width - (POINT_SIZE-1) * 2, POINT_SIZE-1) Y:randRangeAlign(PointArea.origin.y + POINT_SIZE-1, PointArea.size.height - (POINT_SIZE-1) * 2, POINT_SIZE-1)];
		//}
		//while(CGRectIntersectsRect(player.frame, op.frame));
		
		int where = rand() % 4;
		op = [GameOpponent createAtX:(where & 1 ? PointArea.origin.x + OP_SIZE : PointArea.origin.x + PointArea.size.width - OP_SIZE) 
															 Y:(where & 2 ? PointArea.origin.y + OP_SIZE : PointArea.origin.y + PointArea.size.height - OP_SIZE) ];
	}

	// Randomize velocity
	op.velX = OP_VELOCITY * ((rand() % 4) * 2.0 / 3.0 - 1.0);
	op.velY = OP_VELOCITY * ((rand() % 4) * 2.0 / 3.0 - 1.0);
	
	// Add to view
	[self.view addSubview:op];
	[self.view bringSubviewToFront:player];
}

- (void)viewDidLoad {
	loge();
	[super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
	loge();
	[super viewDidAppear:animated];
	
	// Start couting time
	startTime = lastTime = getTime();
	[self updateTimer];
	
	// Reset game state
	pointCount = 0;
	dragged = NO;
	[self updatePointCount];
	
	// Create player
	if(!player) {
		player = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"brickGreen.png"]];
		[self.view addSubview:player];
	}
	player.frame = CGRectMake(160 - PLAYER_SIZE / 2, 240 - PLAYER_SIZE / 2, PLAYER_SIZE, PLAYER_SIZE);
	
	// Create point timer
	inGame = YES;
	[NSThread detachNewThreadSelector:@selector(threadUpdateTimer) toTarget:self withObject:nil];
	[NSThread detachNewThreadSelector:@selector(threadUpdateGameTimer) toTarget:self withObject:nil];
	[NSThread detachNewThreadSelector:@selector(threadNewPointTimer) toTarget:self withObject:nil];
	[NSThread detachNewThreadSelector:@selector(threadNewOpponentTimer) toTarget:self withObject:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
	loge();
	[super viewWillDisappear:animated];
	
	// Remove all points & opponents & player
	[self removeAllPoints];
	[self removeAllOpponents];
	[player removeFromSuperview];
	[player release];
	player = nil;
	inGame = NO;
}

- (void)alertView:(UIAlertView *)myAlertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(!alertView)
		return;
	[alertView release];
	alertView = nil;
	[AppDelegate sharedInstance].settings.name = [[myAlertView textFieldAtIndex:0] text];
	[[AppDelegate sharedInstance].settings addResultForName:[[myAlertView textFieldAtIndex:0] text] Time:lastTime - startTime Score:pointCount];
	[[AppDelegate sharedInstance] menu];
}

- (void)showResults {
	if(alertView)
		return;
	
	alertView = [[UIAlertView alloc] initWithTitle:@"Game Over" 
																											message:[NSString stringWithFormat:@"Time: %.1lf\nScore: %i", lastTime - startTime, pointCount] 
																										 delegate:self cancelButtonTitle:@"Add to highscore" otherButtonTitles:nil];
	[alertView addTextFieldWithValue:[AppDelegate sharedInstance].settings.name label:@"my name"];
	[alertView show];
}

- (void)collideWithPoints {
	BOOL pointsChanged = NO;
	
	for(GamePoint* pt in self.view.subviews) {
		if(![pt isKindOfClass:[GamePoint class]])
			continue;
		
		if(CGRectIntersectsRect(player.frame, pt.frame)) {
			[pt removeFromSuperview];
			++pointCount;
			pointsChanged = YES;
		}
	}
	
	if(pointsChanged)
		[self updatePointCount];
}

- (void)collideWithOpponents {
		if(!inGame)
			return;
		
	for(GameOpponent* op in self.view.subviews) {
		if(![op isKindOfClass:[GameOpponent class]])
			continue;
		
		if(CGRectIntersectsRect(player.frame, op.frame)) {
			inGame = NO;
			
			// Show delayed results
			[NSTimer scheduledTimerWithTimeInterval:DELAY_RESULTS target:self selector:@selector(showResults) userInfo:nil repeats:NO];
		}
	}
}

- (void)updateGame {
	double currentTime = getTime();
	double frameTime = currentTime - lastTime;
	if(frameTime < 0.001)
		return;
	lastTime = currentTime;
	if(frameTime > 1.0)
		frameTime = 1.0;
	
	for(GameOpponent* op in self.view.subviews) {
		if(![op isKindOfClass:[GameOpponent class]])
			continue;
		
		float newPosX = op.posX + op.velX * frameTime;
		float newPosY = op.posY + op.velY * frameTime;
		
		if(newPosX <= OpponentArea.origin.x) {
			newPosX = 2 * OpponentArea.origin.x - newPosX;
			op.velX = -op.velX;
		}
		
		if(newPosY <= OpponentArea.origin.y) {
			newPosY = 2 * OpponentArea.origin.y - newPosY;
			op.velY = -op.velY;
		}
		
		float vx = OpponentArea.origin.x + OpponentArea.size.width;
		if(vx <= newPosX) {
			newPosX = 2 * vx - newPosX;
			op.velX = -op.velX;
		}
		
		float vy = OpponentArea.origin.y + OpponentArea.size.height;
		if(vy <= newPosY) {
			newPosY = 2 * vy - newPosY;
			op.velY = -op.velY;
		}
		
		[op setPosX:newPosX Y:newPosY];
	}
	
	[self collideWithOpponents];
	
	for(GamePoint* pt in self.view.subviews) {
		if(![pt isKindOfClass:[GamePoint class]])
			continue;
		
		pt.pointTime += frameTime;
		
		// Remove
		if(pt.pointTime > POINT_MAX_TIME) {
			[pt removeFromSuperview];
			continue;
		}
		
		// Fade-out
		if(pt.pointTime > POINT_TIME)
			pt.alpha = 1.0f - (pt.pointTime - POINT_TIME) / (POINT_MAX_TIME - POINT_TIME);
		
		// Fade-in
		else
			pt.alpha = pt.pointTime / POINT_TIME;
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch* p in touches) {
		CGPoint pt = [p locationInView:self.view];
		if(CGRectContainsPoint(player.frame, pt)) {
			draggedPoint = pt;
			dragged = YES;
			return;
		}
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if(!dragged || !inGame)
		return;
	
	for(UITouch* p in touches) {
		CGPoint pt = [p previousLocationInView:self.view];
		if(CGPointEqualToPoint(pt, draggedPoint)) {
			pt = [p locationInView:self.view];
			CGRect frame = player.frame;
			frame.origin.x += pt.x - draggedPoint.x;
			frame.origin.y += pt.y - draggedPoint.y;
			player.frame = ClipRectToArea(frame, GameArea);
			draggedPoint = pt;		
			
			// Collide player
			[self collideWithPoints];
			[self collideWithOpponents];
			return;
		}
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if(!dragged)
		return;
	
	for(UITouch* p in touches) {
		CGPoint pt = [p locationInView:self.view];
		if(CGPointEqualToPoint(pt, draggedPoint)) {
			dragged = NO;
			return;
		}
	}
}

- (void)dealloc {
	loge();
	[pointDigit0 release];
	[pointDigit1 release];
	[pointDigit2 release];
	[timeSecondsDigit0 release];
	[timeSecondsDigit1 release];
	[timeMinutesDigit0 release];
	[timeMinutesDigit1 release];
	[player release];
	[alertView release];
	[super dealloc];
}

#pragma mark -- Threads --
- (void)threadUpdateTimer {
	NSAutoreleasePool* p = [[NSAutoreleasePool alloc] init];
	[NSThread setThreadPriority:1];
	while(inGame) {
		[self performSelectorOnMainThread:@selector(updateTimer) withObject:nil waitUntilDone:YES];
		[NSThread sleepForTimeInterval:1.0];
	}
	[p release];
}

- (void)threadUpdateGameTimer {
	NSAutoreleasePool* p = [[NSAutoreleasePool alloc] init];
	[NSThread setThreadPriority:1];
	while(inGame) {
		[self performSelectorOnMainThread:@selector(updateGame) withObject:nil waitUntilDone:YES];
		[NSThread sleepForTimeInterval:1.0f/GAME_FPS];
	}
	[p release];
}

- (void)threadNewPointTimer {
	NSAutoreleasePool* p = [[NSAutoreleasePool alloc] init];
	[NSThread setThreadPriority:1];
	while(inGame) {
		[self performSelectorOnMainThread:@selector(addNewPoint) withObject:nil waitUntilDone:YES];
		[NSThread sleepForTimeInterval:NEW_POINT_TIMER];
	}
	[p release];
}

- (void)threadNewOpponentTimer {
	NSAutoreleasePool* p = [[NSAutoreleasePool alloc] init];
	[NSThread setThreadPriority:1];
	while(inGame) {
		[self performSelectorOnMainThread:@selector(addNewOpponent) withObject:nil waitUntilDone:YES];
		[NSThread sleepForTimeInterval:randRange(NEW_OPPONENT_TIMER, 2.0)];
	}
	[p release];
}

@end
