//
//  CreditsView.m
//  QuickBlock
//
//  Created by ayufan on 10/1/08.
//  Copyright 2008 BinaryCore. All rights reserved.
//

#import "CreditsView.h"
#import "Common.h"

#define ANIMATION_TIME 1.0f/30.0f
#define TOP 200
#define BOTTOM 440
#define SPEED 1

@implementation CreditsView
- (void)viewDidLoad {
	[super viewDidLoad];
	loge();
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	timer = [[NSTimer scheduledTimerWithTimeInterval:ANIMATION_TIME target:self selector:@selector(tick) userInfo:nil repeats:YES] retain];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	[timer invalidate];
	timer = nil;
}

- (void)tick {
	CGRect frame = scrollText.frame;
	frame.origin.y -= SPEED;
	if(frame.origin.y + frame.size.height < TOP)
		frame.origin.y = BOTTOM;
	scrollText.frame = frame;
}

- (void)dealloc {
	loge();
	[timer invalidate];
	[scrollText release];
    [super dealloc];
}
@end
