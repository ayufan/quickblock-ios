/*
 *  Common.h
 *  QuickBlock
 *
 *  Created by ayufan on 9/30/08.
 *  Copyright 2008 BinaryCore. All rights reserved.
 *
 */


#define logf(fmt, args...)	NSLog([NSString stringWithFormat:@fmt, ##args])
#define loge() logf("%s: %p", __FUNCTION__, self)
#define logff(fmt, args...) logf("%s(%i): " fmt, __FUNCTION__, __LINE__, ##args)
#define logo(obj) logf("%s(%i):" #obj ": %p (%i)", __FUNCTION__, __LINE__, obj, obj ? [obj retainCount] : 0)

#define SETTINGS_FILE [@"~/Library/Settings.dat" stringByExpandingTildeInPath]
#define PLAYER_SIZE 46
#define OP_SIZE 32
#define POINT_SIZE 16

#define NEW_POINT_TIMER 0.5
#define NEW_OPPONENT_TIMER 20
#define GAME_FPS 45.0

#define DELAY_RESULTS 0.1

#define OP_VELOCITY 180

#define MAX_RESULTS 10

#define POINT_TIME 0.5
#define POINT_MAX_TIME 4

const static CGRect GameArea = {0, 25, 320, 455};
const static CGRect PointArea = {2, 27, 318, 453};
const static CGRect OpponentArea = {2 + OP_SIZE / 2, 27 + OP_SIZE / 2, 318 - OP_SIZE, 453 - OP_SIZE};

@interface UIAlertView (TextField)
- (UITextField*)textFieldAtIndex:(int)index;
- (void)addTextFieldWithValue:(NSString*)value label:(NSString*)label;
@end
