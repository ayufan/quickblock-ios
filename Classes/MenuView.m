//
//  MenuView.m
//  QuickBlock
//
//  Created by ayufan on 10/1/08.
//  Copyright 2008 BinaryCore. All rights reserved.
//

#import "MenuView.h"
#import "GameSettings.h"
#import "AppDelegate.h"

@implementation MenuView
- (void)dealloc {
    [super dealloc];
}

- (IBAction)showScores {
	NSMutableString* scoresString = [NSMutableString stringWithCapacity:10];
	int place = 1;
	for(GameResult* result in [AppDelegate sharedInstance].settings.results) {
		[scoresString appendFormat:@"%i. %@ got %upt in %.2fs\n", place++, result.name, result.score, result.time];
	}
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Scores" message:([scoresString length] ? scoresString : @"No scores") delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
	[alertView show];
}
@end
